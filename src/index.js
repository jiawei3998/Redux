import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'
import App from './App';
// 引入store
import store from "./store/index";
// 引入Provider链接组件和store
import { Provider } from 'react-redux';

// 利用Provider组件将整个App根组件包裹起来,并在Provider中注入store实例
ReactDOM.render( <Provider store={store}><App /></Provider> , document.getElementById('root'));
