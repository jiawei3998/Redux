import React from 'react';
// 引入connect把数据映射到组件中
import { connect } from 'react-redux';
// 引入常量
// import { ADD,EDIT } from "./store/actionType/index";
// 引入dispatch
import { addNum,editNum,editUser,axyncNum } from "./store/actionDispatch/index";

class App extends React.Component{
  componentDidMount(){
    this.props.axyncNum()
  }
  render(){
    return (
      <div className="App">
       <h1>当前的值是:{this.props.mynum}</h1>
       <h3>操作者：{this.props.user}</h3>
       <hr/>
       {/* <button onClick={this.props.addNum}>++</button> */}
       {/* 传值给Btn子组件 */}
       <But {...this.props}></But>
       {/* <button onClick={this.props.editNum}>--</button> */}
      </div>
    )
  }
}

class But extends React.Component{
  state={
    status:true
  }
  targger=(status)=>{
    if(status){
      this.setState({status:false})
      this.props.editUser('rous')
      console.log(status,'11111')
    }else{
      this.setState({status:true})
      this.props.editUser('jack')
      console.log(status,'22222')
    }
  }
  render(){
    // console.log(this.props)
    return (
      <div className="App">
       <button onClick={this.props.addNum}>++</button>
       <button onClick={this.props.editNum}>--</button>&nbsp;
       <button onClick={this.targger.bind(this,this.state.status)}>改变操作者</button>
      </div>
    )
  }
}

// 获取store中的数据
const mapStateToprops=(state)=>{
  console.log(state)
  // 返回一个对象
  return{
    mynum:state.numReducer.num,
    user:state.userReducer.user
  }
}

// action发送事件
const Appdispath=(dispatch)=>{
  // 返回一个事件
  return {
    addNum:()=>{
      // dispatch({type:ADD,unit:10})
      dispatch(addNum(10))
    },
    editNum:()=>{
      // dispatch({type:EDIT,unit:10})
      dispatch(editNum(10))
    },
    editUser:(user)=>{
      dispatch(editUser(user))
    },
    axyncNum:()=>{
      console.log(axyncNum(),'axyncNum')
      dispatch(axyncNum())
    }
  }
}

// 将数据映射到组件中去
export default connect(mapStateToprops,Appdispath)(App)
