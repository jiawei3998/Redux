
// 引入axios
import axios from 'axios'

// 引入type常量
import { ADD,EDIT,USER,AXYNC } from "../actionType/index";

// 增加的
export const addNum=(unit)=>{
    return{type:ADD,unit}
}

// 减少的
export const editNum=(unit)=>{
    return{type:EDIT,unit}
}

// 修改操作者
export const editUser=(user)=>{
    return{type:USER,user}
}

// 异步请求
export const axyncNum=()=>{
    return (dispatch)=>{
        axios.get('https://easy-mock.com/mock/5cff5f447806440acf2c6856/baseList/')
        .then(res=>dispatch({type:AXYNC,unit:res.data.nums}))
    }
}