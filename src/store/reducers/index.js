
// // 引入常量
// import { ADD, EDIT} from "../actionType/index";

// // 声明一个存储
// let obj={
//     num:10
// }

// // 导出管理员
// export default (state=obj,action)=>{
//     // console.log(action)
//     if(action.type === ADD){
//         // return {num:state.num+action.unit}
//         // 深拷贝  数据增加
//         let nowdata=JSON.parse(JSON.stringify(state))
//         nowdata.num+=action.unit
//         return nowdata
//     }
//     if(action.type === EDIT){
//         // return {num:state.num-action.unit}
//         // 深拷贝
//         let nowdata=JSON.parse(JSON.stringify(state))
//         nowdata.num-=action.unit
//         return nowdata
//     }
//     return state
// }

// 引入不同管理员
import numReducer from "./numReducer";
import userReducer from "./userReducer";
// 引入合并函数管理员的函数combineReducers
import { combineReducers } from 'redux';

// 导出 注意这里是对象的方式传递进去
export default  combineReducers({numReducer,userReducer})