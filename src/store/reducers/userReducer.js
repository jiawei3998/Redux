
// 引入常量
import { USER} from "../actionType/index";

// 声明一个存储
let obj={
    user:'jack'
}

// 导出管理员
export default (state=obj,action)=>{
    if(action.type === USER){
        // console.log(action)
        // 深拷贝
        let nowdata=JSON.parse(JSON.stringify(state))
        nowdata.user=action.user
        return nowdata
    }
    return state
}