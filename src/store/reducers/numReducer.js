
// 引入常量
import { ADD, EDIT,AXYNC} from "../actionType/index";

// 声明一个存储
let obj={
    num:10
}

// 导出管理员
export default (state=obj,action)=>{
    // console.log(action)
    if(action.type === ADD){
        // return {num:state.num+action.unit}
        // 深拷贝  数据增加
        let nowdata=JSON.parse(JSON.stringify(state))
        nowdata.num+=action.unit
        return nowdata
    }
    if(action.type === EDIT){
        // return {num:state.num-action.unit}
        // 深拷贝
        let nowdata=JSON.parse(JSON.stringify(state))
        nowdata.num-=action.unit
        return nowdata
    }
    if(action.type === AXYNC){
        // return {num:state.num-action.unit}
        // 深拷贝
        let nowdata=JSON.parse(JSON.stringify(state))
        nowdata.num=action.unit
        return nowdata
    }
    return state
}